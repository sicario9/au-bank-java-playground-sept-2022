import java.util.Scanner;

public class NewInterest {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int principal=0;
		while(true) {
		System.out.println("Principal (1000-100000):");
		 principal=sc.nextInt();
		if(principal<1000 || principal>10000) {
			System.out.println("only ranges are allow");
			continue;
		}
			
		
		if(principal>=1000 || principal<=100000)
			break;
		}
		
		int periods=0;
		while(true) {
		System.out.println("Periods (Years)(1-20):");
		periods=sc.nextInt();
		if(periods<1 || periods>20) {
			System.out.println(" periods only ranges are allow");
			continue;
		}
			
		if(periods>=1 || periods<=20)
			break;;
		}
		
		float annualInterest=0.0F;
		while(true) {
		System.out.println("AnnualInterest (1-30):");
		annualInterest=sc.nextFloat();
		if(annualInterest<1 || annualInterest>30) {
			System.out.println(" interest only ranges are allow");
			continue;
		}
			
		if(annualInterest>=1 || annualInterest<=30)
			break;
		}
		
		int percent=100;
		int month=12;
		float monthlyInterest= annualInterest/percent/month;
		int totalNumberOfPayments=periods*12;
		
		double mortgage=principal*(monthlyInterest*Math.pow(1+monthlyInterest,totalNumberOfPayments))/(Math.pow(1+monthlyInterest, totalNumberOfPayments)-1);
		
		System.out.println("mortgage "+mortgage );
	}

}
