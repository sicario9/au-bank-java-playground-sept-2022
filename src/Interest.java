import java.util.Scanner;
public class Interest {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Principal:");
		int principal=sc.nextInt();
		
		System.out.println("Periods (Years):");
		int periods=sc.nextInt();
		
		System.out.println("AnnualInterest:");
		float annualInterest=sc.nextFloat();
		
		int percent=100;
		int month=12;
		float monthlyInterest= annualInterest/percent/month;
		int totalNumberOfPayments=periods*12;
		
		double mortgage=principal*(monthlyInterest*Math.pow(1+monthlyInterest,totalNumberOfPayments))/(Math.pow(1+monthlyInterest, totalNumberOfPayments)-1);
		
		System.out.println("mortgage "+mortgage );
	}
}
