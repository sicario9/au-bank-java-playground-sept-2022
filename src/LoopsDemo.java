
public class LoopsDemo {
	public static void main(String[] args) {
		int i=1;
		while(i<=10) {
			System.out.println(i);
			i++;
		}
		System.out.println("----------------");
		int j=2;
		
		do {
			System.out.println(j);
		j++;
			
		}while(j<=6);
		System.out.println("----------------");
		
		int k=3;
		for(;k<11;k++)
			for(int z=1;z<=k;z++)
				System.out.print("**");
		    System.out.print(" ");
	}

}
