
public class DataTypesExamples {
	public static void main(String[] args) {
		int numberOfStudents=32;
		System.out.println(numberOfStudents);
		
		long totalKiloBytesUsed=300123456789L; 
		long totalKiloBytesUsd=300_123456_789L;
	    System.out.println(totalKiloBytesUsed);
	    System.out.println(totalKiloBytesUsd);
	    float temperature=31.45F;
	    System.out.println(temperature);
	    boolean isEligible=true;
	    System.out.println(isEligible);                    
		
	}

}
